# [1.1.0](https://gitlab.com/aleksandras_va/semantic/compare/v1.0.0...v1.1.0) (2021-03-25)


### Features

* change ([fd80284](https://gitlab.com/aleksandras_va/semantic/commit/fd8028463228977fec37ac44737dbc615a69be3d))

# 1.0.0 (2021-03-25)


### Bug Fixes

* add test ([3da127f](https://gitlab.com/aleksandras_va/semantic/commit/3da127fc24618436f8b8e1a857b10490d6cf9c33))
* add test ([e14463a](https://gitlab.com/aleksandras_va/semantic/commit/e14463a27d8b3110e425e7f7699fb7621ddca7c6))
* minor change ([e5ae8c4](https://gitlab.com/aleksandras_va/semantic/commit/e5ae8c4ae1233509684f5927ba3465fd0a8e9860))
* remove test ([c590e65](https://gitlab.com/aleksandras_va/semantic/commit/c590e658a9363b022a6ec367d241d2e48d1f8f55))


### Features

* release-test ([89e0d67](https://gitlab.com/aleksandras_va/semantic/commit/89e0d6788d3a1a879316cb24dfd9b329d5cb3bb8))
