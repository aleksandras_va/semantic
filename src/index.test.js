const add = require('./index');

describe('basic test', () => {
	it('get right sum', () => {
		expect(add(1, 2)).toBe(3);
	});
});
